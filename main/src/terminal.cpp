
#include "terminal.hpp"

namespace uqac::network {

    terminal::terminal(char* port)
    {
        iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0) {
            printf("WSAStartup failed with error: %d\n", iResult);
            return;
        }

    }

    SOCKET terminal::acceptConnexion(SOCKET& ListenSocket)
    {
        printf("Accepting a connection... \n");
        SOCKET soc = INVALID_SOCKET;
        soc = accept(ListenSocket, NULL, NULL);
        if (soc == INVALID_SOCKET) {
            printf("accept failed with error: %d\n", WSAGetLastError());
            //closesocket(ListenSocket);
            WSACleanup();
            return NULL;
        }
        return soc;
    }

    void terminal::close()
    {
        WSACleanup();
    }
    

    terminal::~terminal()
    {
        
    }

}