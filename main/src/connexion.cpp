#include "connexion.hpp"

namespace uqac::network{

    connexion::connexion(SOCKET s, std::vector<OnNewDisconnectionFromServer> callbacks)
    {
        iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0) {
            printf("WSAStartup failed with error: %d\n", iResult);
            return;
        }
        for (auto c : callbacks)
        {
            DisconnectCallbacks.push_back(c);
        }
        Socket = s;
    }

    void connexion::SendData(char* message, int size)
    {
        
        iResult = send(Socket, message, size, 0);
        //iResult = send(ConnectSocket, hello, strlen(hello), 0);
        if (iResult == SOCKET_ERROR) {
            printf("send failed with error: %d\n", WSAGetLastError());
            closesocket(Socket);
            WSACleanup();
            return;
        }
    }
    void connexion::SendData()
    {
        std::string message;
        std::cin >> message;
        if (message == "/quit")
        {
            this->close();
        }
        //message = nom + (std::string)" : " + message;
        //printf("%s\n", &message[0]);
        iResult = send(Socket, &message[0], (int)strlen(&message[0]), 0);
        //iResult = send(ConnectSocket, hello, strlen(hello), 0);
        if (iResult == SOCKET_ERROR) {
            printf("send failed with error: %d\n", WSAGetLastError());
            closesocket(Socket);
            WSACleanup();
            return;
        }
    }

    char* connexion::ReceiveData(std::vector<OnMessageReceived> callbacks)
    {
        char recvbuf[DEFAULT_BUFLEN];
        memset(recvbuf, 0, sizeof(recvbuf));
        if (Socket != INVALID_SOCKET)
        {
            iResult = recv(Socket, recvbuf, DEFAULT_BUFLEN, 0);
            if (iResult > 0)
            {
                for (auto c : callbacks)
                {
                    c(recvbuf, Socket);
                }
            }
            else if (iResult == 0)
            {
                close();
                this->~connexion();
            }
            else
            {
                printf("recv failed with error: %d\n", WSAGetLastError());
                close();
            }
                
        }
        return recvbuf;
    }

    void connexion::close()
    {
        printf("Closing a connection...\n");
        for (auto c : DisconnectCallbacks)
        {
            c(this,Socket);
        }
        iResult = shutdown(Socket, SD_SEND);
        if (iResult == SOCKET_ERROR) {
            printf("connexion shutdown failed with error: %d\n", WSAGetLastError());
            closesocket(Socket);
            WSACleanup();
            return;
        }
        closesocket(Socket);
        WSACleanup();
    }
}