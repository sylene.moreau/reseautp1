#include "UDPConnexion.hpp"
namespace uqac::network{

    UDPConnexion::UDPConnexion()
    {

    }
    UDPConnexion::~UDPConnexion()
    {

    }


    char * UDPConnexion::receiveData(char * buffer, size_t bufferSize,int flags, struct sockaddr *clientAddr, socklen_t *clientAddrLenght)
    {
        int n;
        n=recvfrom(Socket,buffer,bufferSize,flags,clientAddr,clientAddrLenght);
        buffer[n] = '\0';
        return buffer;
    }

    void UDPConnexion::sendData(char * buffer, int bufferSize,int flags, struct sockaddr *clientAddr, socklen_t clientAddrLenght)
    {
       if(sendto(Socket,buffer,bufferSize,flags,clientAddr,clientAddrLenght)<0)
       {
           printf("Unable to send message\n");
            return;
       }
    }
}