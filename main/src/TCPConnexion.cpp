#include "TCPConnexion.hpp"
namespace uqac::network{

    TCPConnexion::TCPConnexion()
    {

    }
    TCPConnexion::~TCPConnexion()
    {

    }

    void TCPConnexion::sendData(char * buffer, int bufferSize){
        iResult = send(Socket, buffer, bufferSize, 0 );
            if (iResult == SOCKET_ERROR) {
                printf("send failed with error: %d\n", WSAGetLastError());
                closesocket(Socket);
                WSACleanup();
            }
            printf("Bytes sent: %d\n", iResult);
    }
    char * TCPConnexion::receiveData(char * buffer, int bufferSize){
        iResult = recv(Socket, buffer, bufferSize, 0);
        if (iResult > 0) {
                printf("Bytes received: %d\n", iResult);
        } else if(iResult == 0)
                printf("Connection closing...\n");
            else{
                printf("recv failed with error: %d\n", WSAGetLastError());
            }
        return buffer;
    }
}