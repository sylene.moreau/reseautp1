

#include <iostream>
#include "network.hpp"

namespace uqac::network{

    void network::Init(std::string addr, std::string portnum)
    {
        serverName = addr;
        port = portnum;
        protocol = IPPROTO_TCP;
    }
    network::network()
    {
        FD_ZERO(&current_sockets);
        
        /*std::string prototemp;
        std::cout << "Indicate the server address for your network:\n";
        std::cin >> serverName;
        std::cout << "Indicate the access port for your network:\n";
        std::cin >> port;
        protocol = IPPROTO_TCP;
        std::cout << "Initializing...\n";*/

        iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0) {
            printf("WSAStartup failed with error: %d\n", iResult);
            return;
        }

        RegisterOnNewDisconnectionFromServer([this](connexion* con, SOCKET soc)
            {
                printf("Cleaning Registries...\n");
                //retirer la socket de l'ensemble
                FD_CLR(soc, &current_sockets);
                
                //laretirer de la liste associan socket et co
                SocketAndAssociatedConnection.erase(soc);
                
                //trouver l'id de la connextion correspondant dans une des map
                int key=-1;
                for (auto& i : ClientConnections) {
                    if (i.second == con) {
                        key = i.first;
                    }
                }
                for (auto& j : ServerConnections) {
                    if (j.second == con) {
                        key = j.first;
                    }
                }

                if (key != -1)
                {
                    FD_CLR(ServerConnections[key]->getSocket(), &current_sockets);

                    for (auto& k : SocketAndAssociatedConnection) {
                        if (k.second == ServerConnections[key]) {
                            SocketAndAssociatedConnection.erase(k.first);
                        }
                    }

                    ClientConnections.erase(key);
                    ServerConnections.erase(key);
                }
            
            });
    }

    network::~network()
    {
        std::cout << "Closing network instance...\n";
        for (auto con : ClientConnections)
        {
            FD_CLR(std::get<1>(con)->getSocket(), &current_sockets);
        }
        for (auto con : ServerConnections)
        {
            FD_CLR(std::get<1>(con)->getSocket(), &current_sockets);
        }
    }


    bool network::initServer()
    {
        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = protocol;
        hints.ai_flags = AI_PASSIVE;

        // Resolve the server address and port
        iResult = getaddrinfo(NULL, &port[0], &hints, &results);
        if (iResult != 0) {
            printf("getaddrinfo failed with error: %d\n", iResult);
            WSACleanup();
            return false;
        }

        ListenSocket = socket(results->ai_family, results->ai_socktype, results->ai_protocol);
        if (ListenSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            freeaddrinfo(results);
            WSACleanup();
            return false;
        }

        iResult = bind(ListenSocket, results->ai_addr, (int)results->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            printf("bind failed with error: %d\n", WSAGetLastError());
            freeaddrinfo(results);
            closesocket(ListenSocket);
            WSACleanup();
            return false;
        }
        freeaddrinfo(results);

        iResult = listen(ListenSocket, SOMAXCONN);

        if (iResult == SOCKET_ERROR) {
            printf("listen failed with error: %d\n", WSAGetLastError());
            closesocket(ListenSocket);
            WSACleanup();
            return false;
        }
        FD_SET(ListenSocket, &current_sockets);
        return true;
    }


    connexion* network::listenForConnection()
    {
        SOCKET c = uqac::network::terminal::acceptConnexion(ListenSocket);
        return CreateServerConnexion(c);
            
    }

    connexion* network::TryConnect()
    {
        struct addrinfo* results = NULL,
            * ptr = NULL,
            hintsc;

        ZeroMemory(&hintsc, sizeof(hintsc));
        hintsc.ai_family = AF_UNSPEC;
        hintsc.ai_socktype = SOCK_STREAM;
        hintsc.ai_protocol = protocol;

        // Resolve the server address and port
        iResult = getaddrinfo(&serverName[0], &port[0], &hintsc, &results);
        if (iResult != 0) {
            printf("getaddrinfo failed with error: %d\n", iResult);
            WSACleanup();
            return NULL;
        }


        SOCKET ConnectSocket= INVALID_SOCKET;
        for (ptr = results; ptr != NULL; ptr = ptr->ai_next) {

            // Create a SOCKET for connecting to server
            
            ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                ptr->ai_protocol);
            if (ConnectSocket == INVALID_SOCKET) {
                printf("socket init failed with error: %ld\n", WSAGetLastError());
                WSACleanup();
                return NULL;
            }

            // Connect to server.
            iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
            
            if (iResult == SOCKET_ERROR) {
                closesocket(ConnectSocket);
                ConnectSocket = INVALID_SOCKET;
                continue;
            }
            break;
        }

        freeaddrinfo(results);

        if (ConnectSocket == INVALID_SOCKET) {
            printf("Unable to connect to server!\n");
            WSACleanup();
            return NULL;
        }
        else
        {
            return CreateClientConnexion(ConnectSocket);
        }
    }

    void network::update()
    {

        FD_ZERO(&receive_ready);
        FD_ZERO(&error);
        receive_ready = current_sockets;
        error = current_sockets;

        int socketCount;

            
        struct timeval tvl = { 0, 50 };
        if (socketCount = select(0, &receive_ready, NULL, &error, NULL) < 0)
        {
            printf("select failed with error: %d\n", WSAGetLastError());
        }
            
        for (int i = 0; i <= socketCount; i++)
        {
            SOCKET Rsock = receive_ready.fd_array[i];
            SOCKET Esock = error.fd_array[i];
            if (FD_ISSET(Esock, &error))
            {
                printf("Error in a socket. Closing it....\n");
                FD_CLR(i, &current_sockets);

                closesocket(i);
            }
            else if (FD_ISSET(Rsock, &receive_ready))
            {
                if (Rsock == ListenSocket)
                {
                    connexion* con =listenForConnection();
                    for (auto c : FunctionsForNewConnectionOnServer)
                    {
                        c(con);
                    }
                }
                else
                {
                    if (Rsock != INVALID_SOCKET)
                    {
                        SocketAndAssociatedConnection[Rsock]->ReceiveData(FunctionsForNewMessageOnServer);
                    }
                }
            }
        }
        
        
    }


    connexion* network::CreateClientConnexion(SOCKET soc)
    {
        auto con = new connexion(soc, FunctionsForNewDisconnectionFromServer);
        ClientConnections.insert(std::make_pair(c_nextId++, con));
        //Sockets.insert(std::make_pair(socket_nextId++, soc));
        
        for (auto c : FunctionsForNewClientConnections)
        {
            c();
        }

        FD_SET(soc, &current_sockets);
        associateSocketAndConnexion(soc, con);
        return con;
    }

    connexion* network::CreateServerConnexion(SOCKET soc)
    {
        auto con = new connexion(soc, FunctionsForNewDisconnectionFromServer);
        ServerConnections.insert(std::make_pair(s_nextId++, con));
        //Sockets.insert(std::make_pair(socket_nextId++, soc));
        
        

        FD_SET(soc, &current_sockets);
        associateSocketAndConnexion(soc, con);
        return con;
    }


    void network::associateSocketAndConnexion(SOCKET soc, connexion* con)
    {
        SocketAndAssociatedConnection.insert(std::make_pair(soc, con));
    }


    void network::RegisterOnNewConnectionOnServer(OnNewConnectionOnServer callback)
    {
        FunctionsForNewConnectionOnServer.push_back(callback);
    }

    void network::RegisterOnNewDisconnectionFromServer(OnNewDisconnectionFromServer callback)
    {
        FunctionsForNewDisconnectionFromServer.push_back(callback);
    }

    void network::RegisterOnNewClientConnection(OnClientConnect callback)
    {
        FunctionsForNewClientConnections.push_back(callback);
    }

    void network::RegisterOnMessageReceived(OnMessageReceived callback)
    {
        FunctionsForNewMessageOnServer.push_back(callback);
    }
    
}
