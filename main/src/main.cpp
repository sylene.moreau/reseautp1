#pragma once

#include <iostream>
#include <thread>
#include <vector> /*a enlever*/
#include "network.hpp"
#include "terminal.hpp"
#include "connexion.hpp"
uqac::network::network net;



void update() {
    net.update();
}
void serveur()
{
    bool InitOK = net.initServer();
    
    std::vector <uqac::network::connexion*> AllPlayers;

    net.RegisterOnNewConnectionOnServer([&AllPlayers](uqac::network::connexion* con)
        {
            //envoyer message qux autre pour dire un nouvvel arrivant
            for (auto c : AllPlayers)
            {
                c->SendData("Someone new has joinded. Say hi!",33);
            }
            AllPlayers.push_back(con);

        });

    net.RegisterOnNewDisconnectionFromServer([&AllPlayers](uqac::network::connexion* con, SOCKET so)
        {
            for (auto c : AllPlayers)
            {
                if (c->getSocket() != so)
                {
                    c->SendData("Someone left. Bye bye...",25);
                }
            }
            AllPlayers.erase(std::remove(AllPlayers.begin(), AllPlayers.end(), con), AllPlayers.end());
            printf("A user left\n");

        });
    net.RegisterOnMessageReceived([](char* s, SOCKET w) {printf("server received a message\n"); });
    if (InitOK)
    {
        std::cout << "Server Started!\n";
        std::thread update(update);
        while (true)
        {

        }
        if (update.joinable())
        {
            update.join();
        }
    }
    return;
}

void client()
{
    net.RegisterOnNewClientConnection([]() {printf("You're connected! Welcome~\n"); });
    net.RegisterOnMessageReceived([](char* s, SOCKET z) {printf("hey there you received a message\n"); });
    uqac::network::connexion* Connexion = net.TryConnect();

    if (Connexion != NULL)
    {
        std::string nom;
        std::cout << "Enter your username : ";
        std::cin >> nom;
        Connexion->setName(nom);
        std::thread update(update);
        char* test = "fin\0 de \0string";
        Connexion->SendData(test,15);
        while (Connexion != NULL)
        {
            Connexion->SendData();
        }
        if (update.joinable())
        {
            update.join();
        }
    }
    
    Connexion->close();
    
    return;
}


int main(int argc, char** argv)
{
    net.Init("localhost", "8080");
        
    std::string type = "";
    do
    {
        std::cout << "Which type of terminal do you want? (C for client | S for Server)\n";
        std::cin >> type;
    } while (!(type == "C" || type == "S"));
        
    if (type == "C")
    {
        std::thread client(client);

        if (client.joinable())
        {
            client.join();
        }
    }
    else if (type == "S")
    {
        std::thread serveur(serveur);

        if (serveur.joinable())
        {
            serveur.join();
        }

    }
    else
        std::cout << "Vous devez uniquement choisir 'S' ou 'C'\n";

    
    return 0;
    
}
