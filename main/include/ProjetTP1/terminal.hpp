#ifndef Terminal_head
#define Terminal_head

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "connexion.hpp"

#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

namespace uqac::network{
    class terminal
    {
    public:
        
        terminal(char* port = "27015");

        ~terminal();
        void close();
        static SOCKET acceptConnexion(SOCKET& ListenSocket);

    private:
        WSADATA wsaData;
        int iResult;


        struct addrinfo* result = NULL;
        struct addrinfo hints;

        int recvbuflen = 512;

    };
}

#endif