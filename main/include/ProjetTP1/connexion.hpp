#ifndef Connexion_head
#define Connexion_head

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <functional>

#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

namespace uqac::network {

    class connexion
    {
    public:
        connexion() = default;
        ~connexion() = default;

        using OnNewDisconnectionFromServer = std::function<void(uqac::network::connexion*, SOCKET)>;
        connexion(SOCKET s, std::vector<OnNewDisconnectionFromServer> callbacks);
        
        void SendData(char*, int size);
        void SendData();
        void close();

        
        using OnMessageReceived = std::function<void(char*,SOCKET)>;
        char* ReceiveData(std::vector<OnMessageReceived>);

        inline void setName(std::string name) { nom = name; };
        inline std::string getName() { return nom; };
        inline SOCKET getSocket() { return Socket; };
    private:
        std::vector<OnNewDisconnectionFromServer> DisconnectCallbacks;
        std::string nom;
        WSADATA wsaData;
        int iResult;
        SOCKET Socket;
    };
}

#endif
