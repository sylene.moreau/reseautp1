
#include "connexion.hpp"

namespace uqac::network{

	class TCPConnexion : connexion
	{	
		public :
		TCPConnexion();
		~TCPConnexion();
		void sendData(char *, int );
        char * receiveData(char *, int);
	};
}