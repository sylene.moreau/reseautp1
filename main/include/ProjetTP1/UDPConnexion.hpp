#include "connexion.hpp"

namespace uqac::network{
	class UDPConnexion : connexion
	{
		public:
		UDPConnexion();
		~UDPConnexion();
        char * receiveData(char * buffer, size_t bufferSize,int flags, struct sockaddr *clientAddr, socklen_t *clientAddrLenght);
		void sendData(char * buffer, int bufferSize,int flags, struct sockaddr *clientAddr, socklen_t clientAddrLenght);
	};
}
