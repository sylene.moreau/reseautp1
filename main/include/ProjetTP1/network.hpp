
#include <map>
#include <memory>

#include "connexion.hpp"
#include "terminal.hpp"

namespace uqac::network{

    class network
    {
    public:
        network();
        
        ~network();

        bool initServer();
        void Init(std::string addr, std::string portnum);

        connexion* network::listenForConnection();
        connexion* network::TryConnect();
        void update();

        connexion* network::CreateClientConnexion(SOCKET soc);
        connexion* network::CreateServerConnexion(SOCKET soc);

        void associateSocketAndConnexion(SOCKET soc, connexion* con);


        using OnNewConnectionOnServer = std::function<void(connexion*)>;
        void RegisterOnNewConnectionOnServer(OnNewConnectionOnServer callback);

        using OnNewDisconnectionFromServer = std::function<void(connexion*, SOCKET)>;
        void RegisterOnNewDisconnectionFromServer(OnNewDisconnectionFromServer callback);

        using OnClientConnect = std::function<void()>;
        void RegisterOnNewClientConnection(OnClientConnect callback);

        using OnMessageReceived = std::function<void(char*, SOCKET)>;
        void RegisterOnMessageReceived(OnMessageReceived callback);


    private:
        IPPROTO protocol;

        std::string serverName;
        std::string port;

        int iResult;

        fd_set current_sockets;
        fd_set receive_ready;
        fd_set error;

        SOCKET ListenSocket = INVALID_SOCKET;
        WSADATA wsaData;

        size_t s_nextId = 0;
        size_t c_nextId = 0;
        size_t socket_nextId = 0;
        
        std::map<uint64_t, connexion*> ClientConnections;
        std::map<uint64_t, connexion*> ServerConnections;

        std::map<SOCKET, connexion*> SocketAndAssociatedConnection;

        std::vector<OnNewConnectionOnServer> FunctionsForNewConnectionOnServer;
        std::vector<OnNewDisconnectionFromServer> FunctionsForNewDisconnectionFromServer;
        std::vector<OnClientConnect> FunctionsForNewClientConnections;
        std::vector<OnMessageReceived> FunctionsForNewMessageOnServer;
        std::vector<OnMessageReceived> FunctionsForNewMessageOnClient;

        struct addrinfo hints;
        struct addrinfo* results = NULL;

    };
}
