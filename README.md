# Reseau TP1

## Comment utiliser notre chat
Sur une même machine, il suffit de démarrer plusieurs exécutables et de rentrer les informations demandées en prompt. Il faut d'abord lancer la première instance en choisissant S, et toutes les autres en choisissant C. Tout les clients connectés pourront ainsi discuter entre eux.

## Question
### Quel protocole choisi-t-on pour le chat du jeu vidéo ? UDP ou TCP et pourquoi ?
Pour un chat, la latence/bande passante parait moins importante que la perte de données. Donc TCP est le bon choix à faire.

## Avancement
Nous n'avons malheureusement pas eu le temps d'implémenter le TCP/UDP, même si nous avions créer et commencer les classes dans des commits précédents.
